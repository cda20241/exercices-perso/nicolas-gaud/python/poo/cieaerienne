class Siege:
    _num_allee: str = ""
    _num_rangee: str = ""
    _billet = []

    def __init__(self, num_allee, num_rangee):
        self._num_allee = num_allee
        self._num_rangee = num_rangee

    @property
    def num_allee(self):
        return self._num_allee

    @num_allee.setter
    def num_allee(self, new_num_allee):
        self._num_allee = new_num_allee

    @property
    def num_rangee(self):
        return self._num_rangee

    @num_rangee.setter
    def num_rangee(self, new_num_rangee):
        self._num_rangee = new_num_rangee

    @property
    def billet(self):
        return self._billet

    @billet.setter
    def billet(self, new_billet):
        self._billet = new_billet
