from Avion import Avion
from Compagnie import Compagnie
from datetime import datetime

from Passager import Passager
from Siege import Siege

compagnie1 = Compagnie("A")
compagnie2 = Compagnie("B")
avion = Avion("AirPlane", 10)
compagnie1.creerVol("Berlin", "Tokyo", datetime(2023, 10, 17),
datetime(2023, 10, 17), avion)
compagnie1.creerVol("Paris", "Tokyo", datetime(2023, 10, 17),
datetime(2023, 10, 17), avion)
# compagnie1.afficherVols()
# volsParis = compagnie1.getVolsByVilleDepart("Paris")
# for vol in volsParis:
#    compagnie1.annulerVol(vol)
#
# print("Vols pour Paris annulés")
# compagnie1.afficherVols()

# Création de 3 billets pour le premier vol de la compagnie1
for i in range(0,3):
    compagnie1.vols[0].creerBillet(50, compagnie1.vols[0].avion.sieges[i])

isFree = False
place = ""
siege = None
vols = compagnie1.vols
avion = vols[0].avion

# Menu et vérification de l'attribution d'un siège entré par l'utilisateur
while not isFree:
    print("Veuillez choisir une place : ")
    place = input()
    # Bugging
    if not vols[0].checkSeatExists(Siege(place[0], place[1]), avion.sieges):
        print("Ce siege nexiste pas !")
    else:
        siege = compagnie1.vols[0].attribuerSiege(place, compagnie1.vols[0].rechercherPlacesLibres())
    if siege is not None:
        print("Ce siege est libre !")
        isFree = True
    else:
        print("Ce siege est déjà attribué !")

passager = Passager("DUPONT", "Jean")
passager.billets.append(compagnie1.vols[0].creerBillet(50, siege))
print("Votre billet a bien été créé.")

avionREmplacement = Avion("AirForceOne", 20)
compagnie1.changerAvionVol("AirPlane", avionREmplacement)
print("L'avion \"AirPlane\" a été remplacé par l'avion \"AirForceOne\".")
