class Billet:
    _prix = ""
    _siege = None

    def __init__(self, prix, siege):
        self._prix = prix
        self._siege = siege

    @property
    def prix(self):
        return self._prix

    @prix.setter
    def prix(self, new_prix):
        self._prix = new_prix

    @property
    def siege(self):
        return self._siege

    @siege.setter
    def siege(self, new_siege):
        self._siege = new_siege
