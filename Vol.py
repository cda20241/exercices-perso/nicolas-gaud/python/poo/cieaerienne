from datetime import date

from Billet import Billet


class Vol:
    def __init__(self, numVol: int, dateDepart: date, dateArrivee: date, villeDepart: str, villeArrivee: str, compagnie,
                 avion):
        self._numVol = numVol
        self._dateDepart = dateDepart
        self._dateArrivee = dateArrivee
        self._villeDepart = villeDepart
        self._villeArrivee = villeArrivee
        self._compagnie = compagnie
        self._avion = avion
        self._billets = []

    @property
    def numVol(self):
        return self._numVol

    @numVol.setter
    def numVol(self, value):
        self._numVol = value

    @property
    def dateDepart(self):
        return self._dateDepart

    @dateDepart.setter
    def dateDepart(self, value):
        self._dateDepart = value

    @property
    def dateArrivee(self):
        return self._dateArrivee

    @dateArrivee.setter
    def dateArrivee(self, value):
        self._dateArrivee = value

    @property
    def villeDepart(self):
        return self._villeDepart

    @villeDepart.setter
    def villeDepart(self, value):
        self._villeDepart = value

    @property
    def villeArrivee(self):
        return self._villeArrivee

    @villeArrivee.setter
    def villeArrivee(self, value):
        self._villeArrivee = value

    @property
    def compagnie(self):
        return self._compagnie

    @compagnie.setter
    def compagnie(self, value):
        self._compagnie = value

    @property
    def avion(self):
        return self._avion

    @avion.setter
    def avion(self, value):
        self._avion = value

    @property
    def billets(self):
        return self._billets

    @billets.setter
    def billets(self, billets):
        self._billets = billets

    def creerBillet(self, prix, siege):
        billet = Billet(prix, siege)
        self._billets.append(billet)
        return billet

    def afficherBillets(self):
        for billet in self._billets:
            print(billet.siege.num_allee + " : " + billet.siege.num_rangee)

    """
    Cette fonction compare les sièges attribués dans les billets du vol avec ceux de l'avion du vol
    
    et affiche ceux qui ne sont pas déjà attribués
    """

    def rechercherPlacesLibres(self):
        sieges = self.avion._sieges
        sieges_tmp = sieges
        for billet in self._billets:
            for siege in sieges:
                if str(billet.siege.num_allee) + str(billet.siege.num_rangee) == str(siege.num_allee) + str(siege.num_rangee):
                    sieges_tmp.remove(siege)
        sieges = sieges_tmp
        return sieges

    """
    Cette fonction vérifie si un siège entré par l'utilisateur et passé en paramètre existe dans l'avion affecté au vol
    
    :param siege: Siege: le siège à vérifier
    
    :param sieges: Siege[]: la liste de sièges dans laquelle rechercher

    :return: a boolean

    """

    def checkSeatExists(self, siege, sieges):
        exists = False
        for siege_tmp in sieges:
            if siege_tmp.num_allee == siege.num_allee and siege_tmp.num_rangee == siege.num_rangee:
                exists = True
                break
        return exists

    """
     Cette fonction verifie que le siege definit par "place" entré par l'utilisateur existe dans la liste de sièges
     
     non attribués passée en paramètre
     
     :param place : String
     
     * @param sieges : Siege[]
     
     * @return Siege
    """

    def attribuerSiege(self, place, sieges):
        a = place[0]
        b = place[1]
        for tmpSiege in sieges:
            if tmpSiege.num_allee == a and tmpSiege.num_rangee == b:
                return tmpSiege
        return None
