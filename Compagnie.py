from Billet import Billet
from Vol import Vol


class Compagnie:
    _nom = ""
    _vols = []
    _num_vol = 0

    def __init__(self, nom):
        self._nom = nom

    @property
    def nom(self):
        return self._nom

    @nom.setter
    def nom(self, new_nom):
        self._nom = new_nom

    @property
    def vols(self):
        return self._vols

    @vols.setter
    def vols(self, new_vols):
        self._vols = new_vols

    @property
    def num_vol(self):
        return self._vols

    @num_vol.setter
    def num_vol(self, new_num_vol):
        self._num_vol = new_num_vol

    def creerVol(self, villeDepart, villeArrivee, dateDepart, dateArrivee,avion):
        self._num_vol += 1
        vol = Vol(self.num_vol, dateDepart, dateArrivee, villeDepart, villeArrivee, self, avion)
        self.vols.append(vol)
        # vol.compagnie(self)

    def annulerVol(self, vol_annule):
        self.vols.remove(vol_annule)
        vol_annule.setCompagnie(None)

    def afficherVols(self):
        for vol in self.vols:
            print(vol)

    def getVolsByVilleDepart(self, villeDepart):
        listeVols = []
        for vol in self.vols:
            if vol.villeDepart == villeDepart:
                listeVols.append(vol)
        return listeVols

    """
    Si la compagnie ne peut pas proposer de siège au client pour le vol demandé,
    alors la fonction retourne null.
    Si on peut proposer un billet pour le vol, alors on renvoie un billet, et on
    place le client sur le premier siège disponible dans l'avion, on ne permet pas
    au client de choisir lui-même son siège.
    """
    def commanderBillet(self, volAReserver):
        billet = Billet(50, None)
        volDeCompagnie = False
        for vol in self.vols:
            if vol == volAReserver:
                volDeCompagnie = True

        if volDeCompagnie == False:
            return None

        # je me suis assuré que le vol à réserver est bien affreté
        # par cette compagnie

        avionAffretePourLeVol = volAReserver.avion
        sieges = avionAffretePourLeVol.sieges
        # je récupère la liste des sièges de l'avion associé à ce vol,
        # et je vais regarder s'il y a un siège disponible
        siegeDisponible = False
        for siege in sieges:
            if siege.billet == None:
                # dès que je trouve un siège disponible, je l'associe à mon billet
                # // je ne laisse pas la possibilité au client de choisir son billet je
                # // lui mets le premier siège disponible
                siegeDisponible = True
                billet.siege = siege
                siege.billet = billet
        if siegeDisponible == True:
            return billet
        else:
            return None

    def changerAvionVol(self, modele, avion):
        for vol in self.vols:
            if vol.avion.modele == modele:
                vol.avion = avion
