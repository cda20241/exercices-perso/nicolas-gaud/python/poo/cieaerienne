from datetime import date


class Passager:
    def __init__(self, nom, prenom):
        self._nom = nom
        self._prenom = prenom
        self._billets = []

    @property
    def nom(self):
        return self._nom

    @nom.setter
    def nom(self, value):
        self._nom = value

    @property
    def prenom(self):
        return self._prenom

    @prenom.setter
    def prenom(self, value):
        self._prenom = value

    @property
    def dateNaissance(self):
        return self._dateNaissance

    @dateNaissance.setter
    def dateNaissance(self, value):
        self._dateNaissance = value

    @property
    def telephone(self):
        return self._telephone

    @telephone.setter
    def telephone(self, value):
        self._telephone = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value

    @property
    def billets(self):
        return self._billets

    @billets.setter
    def billets(self, billets):
        self._billets = billets