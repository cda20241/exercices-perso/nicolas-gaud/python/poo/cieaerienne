from Siege import Siege


class Avion:
    _modele = ""
    _sieges = []

    def __init__(self, modele, nb_rangees):
        self._modele = modele
        # Création des sièges A1 - A -> nbRangees à I1 - I -> nbRangees
        # 65 = "A" et 73 = "I" en Ascii décimal
        for i in range(65, 74):
            for j in range(1, nb_rangees + 1):
                siege = Siege(chr(i), str(j))
                self._sieges.append(siege)

    @property
    def modele(self):
        return self._modele

    @modele.setter
    def modele(self, new_modele):
        self._modele = new_modele

    @property
    def sieges(self):
        return self._sieges

    @sieges.setter
    def sieges(self, new_sieges):
        self._sieges = new_sieges

    def afficher_sieges(self):
        for siege in self._sieges:
            print(siege.num_allee + " : " + siege.num_rangee)






